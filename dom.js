function dom(){

    var text = document.querySelector("input");
    var arr = document.querySelector('ol');
    var listaLi = document.createElement('li');
    listaLi.innerHTML = text.value;
    listaLi.addEventListener('click', remove);
    arr.appendChild(listaLi);
    text.value="";
}

function remove(element) {
    element.target.remove();
}


function edit(){
   
    let pos = document.getElementById('pos');
    pos = pos.value;
    let arr = document.querySelector('ol');
    let tot = arr.childElementCount;
    if(pos < 0 || pos > tot){
        alert("A posicao digitada nao existe");
        return;
    }
    let item = document.querySelector(`ol>li:nth-child(${pos})`)
    let txt = document.getElementById('novoText');
    item.innerHTML=txt.value;
}
